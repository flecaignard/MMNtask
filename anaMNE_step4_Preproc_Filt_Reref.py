#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#
# STEP 4 : The aim here is to filter and resample data, and re-reference to avg mastoids
#           - notch filter (60 , 120 and 180 Hz)
#           - high-pass: 1 Hz
#           - downsample to 512 Hz (so that 0.5 - 128 is fine for analyses)
#           - re-ref to avg(M1,M2)

#
# assummes Step1 : Step3 have been conducted
# Requires *.renamed.raw-eve.fif and *.preproc_bad.raw.fif [events, raw]
#
# Output : *.preproc_filt.raw.fif, *.resampled_renamed.raw-eve.fif
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import warnings

from mne import Epochs, pick_types, find_events, write_events, read_events, Annotations, read_selection
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import find_eog_events


print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')
from mmntask_events import mmntask_plot_events, mmntask_recog_condition, mmntask_recode_std

warnings.filterwarnings("ignore",category=DeprecationWarning)

matplotlib qt
# matplotlib qt # from oussama, allows plots in separate windows

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',		'50023_MMTI_MMN', '50012 MMTI_MMN',		'50025_MMTI_MMN'] # supp of two datasets (uncompleted)
nr_subj = len(rawdata_filepref)


raw_eve_filename =[]              # List of event filenames (MNE *recode.raw-eve.fif),
raw_mne_filename =[]              # List of raw data filenames (MNE *.prep1.raw.fif)
new_raw_mne_filename =[]
new_raw_eve_filename =[]            # downsampled event (output)
for i in range(nr_subj) :
    raw_eve_filename.append(os.path.join(dir_mne, '{}.renamed.raw-eve.fif'.format(rawdata_filepref[i])))
    raw_mne_filename.append(os.path.join(dir_mne, '{}.preproc_bad.raw.fif'.format(rawdata_filepref[i])))
    new_raw_mne_filename.append(os.path.join(dir_mne, '{}.preproc_filt.raw.fif'.format(rawdata_filepref[i])))
    new_raw_eve_filename.append(os.path.join(dir_mne, '{}.resampled_renamed.raw-eve.fif'.format(rawdata_filepref[i])))
print(raw_mne_filename)
print(raw_eve_filename)


#############################################################################################
## INIT                                         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# init color for 2D plots
dict_color_sens = dict( eeg='k', eog='blue', stim='m')
dict_color_event = {10: 'magenta', 11: 'black', 12: 'cyan', 20: 'red', 21: 'black', 22:'green', 65790:'yellow'}
dict_color_eog_event = {998: 'magenta'}

# init electrodes order for the viewer
order_eeg = np.array([1, 30, 2, 29, 3, 4, 31, 27, 28, 6,
                      5, 39, 26, 25, 7, 8, 32, 23, 24, 37,
                      10, 9, 22, 21, 38, 11, 12, 13, 19, 20,
                      14, 15,  16, 17, 18,40, 33, 34, 35, 36])
order_eeg = order_eeg -1




#############################################################################################
## Load one subject                             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

id_su=0 #to be adjusted manually, as the screening of individual data is performed
    
f_raw = raw_mne_filename[id_su]
f_new_raw = new_raw_mne_filename[id_su]
f_eve = raw_eve_filename[id_su]
f_new_eve = new_raw_eve_filename[id_su]
raw = read_raw_fif(f_raw)
events =read_events(f_eve)

        
# launch viewer
raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=10, highpass = 2)


#############################################################################################
## Compute Spectrum                            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
# from  MNE example

tmin, tmax = 450, 600 
fmin, fmax = 0, 600
n_fft = 4096

picks = pick_types(raw.info, meg=False, eeg=True, eog=True,stim=False, exclude='bads')
raw.plot_psd(area_mode='range', tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax,n_fft=n_fft, picks=picks, average=True) #ax.plot(np.random.rand(5))
plt.title('raw spectrum')

#############################################################################################
## Notch 60 Hz                            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
# from  MNE example

new_raw= read_raw_fif(f_raw, preload=True)
notch_val = np.arange(60, 241, 60)
picks = pick_types(raw.info, meg=False, eeg=True, eog=True,stim=False, exclude='bads')
new_raw.notch_filter(notch_val, picks=picks, filter_length='auto', phase='zero')


# view spectra
tmin, tmax = 450, 600 
fmin, fmax = 0, 600
n_fft = 4096
raw.plot_psd(area_mode='range', tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax,n_fft=n_fft, picks=picks, average=True) #ax.plot(np.random.rand(5))
plt.title('unfiltered')
new_raw.plot_psd(area_mode='range', tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax,n_fft=n_fft, picks=picks, average=True) #ax.plot(np.random.rand(5))
plt.title('filtered')

new_raw.save(fname=f_new_raw,overwrite=True)


#############################################################################################
## Downsampling to 512 Hz (1024/2)  : data AND events             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
# this is ok wrt events since these are expressed in ms and not in sample
# as a consequence we decrease the precision of about 1ms (=1000/1024) , which is fine

#new_raw.resample(512, npad="auto")  # set sampling frequency to 512 Hz

new_raw, new_events = new_raw.resample(512, npad='auto', events=events)
new_raw.save(fname=f_new_raw,overwrite=True)
write_events(f_new_eve, new_events)


# control
new_raw = read_raw_fif(f_new_raw)
new_events =read_events(f_new_eve)

new_raw.plot(events=new_events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=10, highpass = None)
# new_raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=10, highpass = None)

for i in range(len(new_events)-1):
    hi = events[i,0]/1024
    lo = new_events[i,0]/512
    print(hi, lo, (hi-lo)*1000, events[i,2], new_events[i,2]) # diff = 0.976 ms du to the downsawmpling, loss of precision in event location = fine



#############################################################################################
## bandpass  0.5 - 50 Hz                           -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
new_raw = read_raw_fif(f_new_raw, preload=True)

picks = pick_types(new_raw.info, meg=False, eeg=True, eog=True,stim=False, exclude='bads')
new_raw.filter(1,50, fir_design='firwin', picks=picks)

tmin, tmax = 450, 600 
fmin, fmax = 0,100
n_fft = 4096
new_raw.plot_psd(area_mode='range', tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax,n_fft=n_fft, picks=picks, average=True) #ax.plot(np.random.rand(5))
plt.title('filtered')

new_raw.save(fname=f_new_raw,overwrite=True)



picks_eog = pick_types(raw.info, meg=False, eeg=False, eog=True,stim=False, exclude='bads')
new_raw.plot_psd(area_mode='range', tmin=tmin, tmax=tmax, fmin=fmin,fmax=fmax,n_fft=n_fft, picks=picks_eog, average=True) #ax.plot(np.random.rand(5))
#############################################################################################
## Re-referencing to averaged mastoids          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
new_raw.set_eeg_reference( ref_channels=['M1', 'M2'],  projection=None, verbose=True)
new_raw.plot(events=new_events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = False, order=order_eeg, duration=30, highpass = None)
new_raw.save(fname=f_new_raw,overwrite=True)

