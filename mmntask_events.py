import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events

print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')

#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* mmntask_plot_events  o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
# matplotlib qt

def mmntask_plot_events(events, event_id, color_id, flag_show ):
#
    # event_id = {'std': 1, 'dev': 2, 'start': 65790, 's': 65536}
    # color_id = {1: 'black', 2: 'yellow', 65790: 'red', 65536: 'c'}
    #matplotlib qt

    events_code = np.unique(events[:,2]).tolist()
    event_id_indiv = {k:v for k, v in event_id.items() if v in events_code}
    color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
    fig = plot_events(events, color=color_id_indiv, event_id=event_id_indiv, show=flag_show)
    print('youpi ya')
    return fig


    # return s, c


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* mmntask_recog_condition o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

# bloc_evt = np.darray; vector; contains 1 and 2 events within a bloc (length = 674)
# this function identifies whether bloc_evt correspond to E-condition or U-condition

def mmntask_recog_condition(bloc_evt, code_dev):
    ind_dev = np.where(bloc_evt==code_dev)   # tuple
    diff = ind_dev[0][1] - ind_dev[0][0] - 1
    if ind_dev[0][0]==2 and diff == 3 :
        cond = 'E'
    else :
        cond = 'U'
    return cond





#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* mmntask_recode_std o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################

# inputs =
# bloc_code   : np.array, vector of std and dev codes (1,2)
# cond = string, 'E' if condition expected, 'U' if unexpected
# code_dev = int, code for deviants in raw data (2)

def mmntask_recode_std(bloc_code, cond, code_dev):

    if cond == 'E': #expected
        offset=10
    elif cond == 'U':
        offset=20
    new_bloc_code = bloc_code+offset
    code_dev = code_dev+offset
    ind = np.where(new_bloc_code==code_dev)[0]
    ind =ind -1
    new_bloc_code[ind]=0+offset
    return new_bloc_code
