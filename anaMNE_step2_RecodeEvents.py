#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#
# STEP 2 : recode events across conditions, sort standard preceding a deviant
# Assumes that STEP 1 has been done (*.raw.fif and *.raw-eve.fif exist)
#
# Output : a file *.recode.raw-eve.fif is created with the followin code_s
# event_id_recode = {'E_std': 10, 'E_other': 11, 'E_dev': 12,'U_std': 20, 'U_other': 21, 'U_dev': 22, 'start': 65790} 

#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events


print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')
from mmntask_events import mmntask_plot_events, mmntask_recog_condition, mmntask_recode_std



# matplotlib qt # from oussama, allows plots in separate windows

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_rawdata = '/Users/francoise/Documents/Projets/RickAdams/EEG_RawData'
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',		'50023_MMTI_MMN', '50012 MMTI_MMN',		'50025_MMTI_MMN'] # supp of two datasets (uncompleted)
nr_subj = len(rawdata_filepref)


raw_mne_filename =[]              # List of raw data filenames (MNE *raw.fif), raw here refers to MNE jargon
raw_eve_filename =[]              # List of event filenames (MNE *raw-eve.fif),
recode_raw_eve_filename =[]              # List of event filenames (MNE *recode.raw-eve.fif),
for i in range(nr_subj) :
    raw_mne_filename.append(os.path.join(dir_mne, '{}.raw.fif'.format(rawdata_filepref[i])))
    raw_eve_filename.append(os.path.join(dir_mne, '{}.raw-eve.fif'.format(rawdata_filepref[i])))
    recode_raw_eve_filename.append(os.path.join(dir_mne, '{}.renamed.raw-eve.fif'.format(rawdata_filepref[i])))

print(raw_mne_filename)
print(raw_eve_filename)
print(recode_raw_eve_filename)


#############################################################################################
## Load and recode raw event file (*.raw-eve.fif)-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
# we want 6 different events + start ones
# E = expected; U = unexpected
# std = preceding a deviant; dev = deviant; other = standard from every position excepting pre-deviant ones

event_id_raw = {'std': 1, 'dev': 2, 'start': 65790, 's': 65536}
color_id_raw = {1: 'black', 2: 'yellow', 65790: 'red', 65536: 'c'}

event_id_recode = {'E_std': 10, 'E_other': 11, 'E_dev': 12,'U_std': 20, 'U_other': 21, 'U_dev': 22, 'start': 65790} #general, for all subjects
color_id_recode = {10: 'magenta', 11: 'black', 12: 'cyan', 20: 'red', 21: 'black', 22:'green', 65790:'yellow'} #general, for all subjects


code_start = 65790
code_s = 65536
code_dev = 2


# test on one subject
for id_su in range(nr_subj):

    f = raw_eve_filename[id_su]
    events =read_events(f)
    fig = mmntask_plot_events(events,event_id_raw, color_id_raw,  flag_show=False  )
    plt.title(' {} : raw events'.format(rawdata_filepref[id_su]))
    #
    # --------- find irrelevant code_s, and if exists then suppression

    ind = (events[:,2]==code_s)
    keep = ~ind[:]
    events_new = events[keep[:],:]

    # --------- find code_start at the end of events_new:
    # Indeed, in some cases, an extra code_start has been created at the end of the block
    # So we suppress it in this case (so that there are 4 code_start, because of 4 blocs!)

    keep = np.arange(0,len(events_new)-1)
    if events_new[-1,2] == code_start:
        events_new = events_new[keep[:],:]

    fig = mmntask_plot_events(events_new, event_id_raw, color_id_raw, flag_show=False)
    plt.title('{} : raw events, rm irr'.format(rawdata_filepref[id_su]))



    # -------- Within each bloc, recode events ( namely, identify condition (E/U) and extract std preceding a deviant)
    ind = np.where(events_new[:,2]==code_start)[0]     # np.where gives a tuple, and [0] moves it in an array

    for i  in range(len(ind)) :
        if i < len(ind)-1 : # i = 0,1,2 but not 3
            curs_1 = ind[i]+1
            curs_2 = ind[i+1]
        else:
            curs_1 = ind[i]+1
            curs_2 = len(events_new)
        print(i, curs_1, curs_2-1, curs_2-curs_1 )
        bloc_ind = np.arange(curs_1,curs_2)
        bloc_code = events_new[bloc_ind,2]
        cond = mmntask_recog_condition(bloc_code, code_dev)
        print('bloc', i+1,cond)
        new_bloc_code = mmntask_recode_std(bloc_code, cond, code_dev)
        events_new[bloc_ind,2]=new_bloc_code

    write_events(recode_raw_eve_filename[id_su],events_new)
    f = recode_raw_eve_filename[id_su]
    events =read_events(f)
    fig = mmntask_plot_events(events, event_id_recode, color_id_recode, flag_show=False)
    plt.title('{} : Recode events'.format(rawdata_filepref[id_su]))



plt.show()



# # prep function mmntask_recode_std
# # code_bloc, cond, code_dev, ind_block
# new_bloc_code = bloc_code.copy()
# if cond == 'E': #expected
#     offset=10
# elif cond == 'U':
#     offset=20
# new_bloc_code = bloc_code+offset
# code_dev = code_dev+offset
# ind = np.where(new_bloc_code==code_dev)[0]
# ind =ind -1
# new_bloc_code[ind]=3+offset






#
#
#
# # f = raw_mne_filename[1]
# # raw = read_raw_fif(f, preload=True)
# # raw.info
# # raw.ch_names
# # raw.plot()
# # events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
# # events.shape
# # print(events)
# #
# # picks = pick_types(raw.info, meg=False, eeg=False, stim=True, eog=False, exclude='bads')
# # raw.plot(events=events, order=picks, duration=60) # plot of stim channel only, thanks to 'order'
# # type(events)
# #
# # events_code = np.unique(events[:,2]).tolist()   #list of the diffent event code found in events; tolist : from np.array to list
# # event_id = {'std': 1, 'dev': 2, 'start': 65790, 's': 65536} #general, for all subjects
# # event_id_indiv = {k:v for k, v in event_id.items() if v in events_code} #subset of event codes, adjusted for  current participant
# #
# #
# # color_id = {1: 'green', 2: 'yellow', 65790: 'red', 65536: 'c'}
# # color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
# #
# # plot_events(events, raw.info['sfreq'], raw.first_samp, color=color_id_indiv, event_id=event_id_indiv) # nice plot!
# # write_events(raw_eve_filename[0],events)
# #
#
# # matplotlib qt
# for i,f in  enumerate(raw_eve_filename) :
#     events =read_events(f)
#     events_code = np.unique(events[:,2]).tolist()
#     event_id_indiv = {k:v for k, v in event_id.items() if v in events_code}
#     color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
#     plot_events(events, color=color_id_indiv, event_id=event_id_indiv)
#
#
#
# #############################################################################################
# #############################################################################################
#
# # raw_fname = data_path + '/MEG/sample/sample_audvis_filt-0-40_raw.fif'
# # event_fname = data_path + '/MEG/sample/sample_audvis_filt-0-40_raw-eve.fif'
# # tmin, tmax = -0.1, 0.3
# # event_id = dict(aud_l=1, aud_r=2, vis_l=3, vis_r=4)
# #
# # raw = mne.io.read_raw_fif(raw_fname, preload=True)
# # raw.filter(1, 20, fir_design='firwin')
# # events = mne.read_events(event_fname)
# #
# # picks = mne.pick_types(raw.info, meg=False, eeg=True, stim=False, eog=False,
# #                        exclude='bads')
# #
# # epochs = mne.Epochs(raw, events, event_id, tmin, tmax, proj=False,
# #                     picks=picks, baseline=None, preload=True,
# #                     verbose=False)
# #
# # X = epochs.get_data()


