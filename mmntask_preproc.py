#F.Lecaignard, April 2018

import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import os.path as op
import time

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import ICA

print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')


#############################################################################################
##  o*o*o*o*o*o*o*o*o*o*o*o*o*o* mmntask_plot_events  o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*o*
#############################################################################################
# matplotlib qt

def mmntask_runica_eeg(raw_path, rawdata_filepref, raw=None, start=None, end=None, Fmin = None, Fmax = None , save=True, fit_ica=False, n_components=0.975, method='fastica', ica_rejection={'eeg':20e-05}, EOG_threshold=3, EOG_min=1, EOG_max=2):
    # code adapted from Benjamin Ador: preproc.py 
    
    """
    Dedicated to EOG artifact correction only (no ECG)
    Fit ICA on raw EEG data and return ICA object.
    If save, save ICA, save EOG artifact scores plots, and write log (default to True).
    If fit_ica, fit ICA even if there is already an ICA file (default to False).
    Output:
        '<raw_path>/ICA/<subject>_components-ica.fif'
        '<raw_path>/ICA/<subject>_components-scores_ecg.svg'
        '<raw_path>/ICA/<subject>_components-scores_eog.svg'
    Log:
        '<raw_path>/ICA/<subject>_ICA_log.tsv'
    Parameters (see mne.preprocessing.ICA):
        raw: raw data to fit ICA on. If None (default), will be loaded according to previous parameters.
        n_components: number of components used for ICA decomposition
        method: the ICA method to use
        ica_rejection: epoch rejection threshold (default to 4000 fT for magnetometers)
        EOG_threshold: EOG artifact detection threshold (mne default to 3.0)
        EOG_min: minimum number of EOG components to detect and exclude (default to 1)
    """

#    raw_path = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'
#    rawdata_filepref = '50002_MMTI_MMN.prep2'
#    start = 11 # in sec, manually defined from visual inspection
#    end = 415
#    save=True 
#    fit_ica=False
#    n_components=0.975 
#    method='fastica' 
#    ica_rejection={'eeg':20e-05} 
#    EOG_threshold=3 
#    EOG_min=1
#    EOG_max=2
#    
    
    # Load data
    raw_fname = op.join(raw_path, '{}.raw.fif'.format(rawdata_filepref))
    if not raw:
        raw = read_raw_fif(raw_fname, preload=True)
    
    
    # ICA path
    ICA_path = '{}/ICA'.format(raw_path) #op.join(raw_path, 'ICA')
    print('youpi', ICA_path)
    if save and not op.isdir(ICA_path):
        os.makedirs(ICA_path)
    ICA_file = '{}/{}.components-ica.fif'.format(ICA_path,rawdata_filepref) # op.join(ICA_path, '{}.components-ica.fif'.format(rawdata_filepref))
    print(ICA_file)
    ICA_eog_scores = '{}/{}.ICA_eog_scores.pdf'.format(ICA_path,rawdata_filepref)#op.join(ICA_path, '{}.ICA_eog_scores.pdf'.format(rawdata_filepref))
    print(ICA_eog_scores)
    # ICA log
    ICA_log = '{}/{}.ICA_log.tsv'.format(ICA_path,rawdata_filepref)#op.join(ICA_path, '{}.ICA_log.tsv'.format(rawdata_filepref))
    print(ICA_log)
    if save and not op.isfile(ICA_log):
        with open(ICA_log, 'w') as fid:
            fid.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format('date','time','subject','start','end','n_components','n_selected_comps','ncomp_EOG','rejection','dropped_epochs'))
            
    
    # Crop recording : defines the ICA time interval
    if not start:
        start = 0
    if not end:
        end =  raw.last_samp/raw.info['sfreq']-1
    
    raw.crop(tmin=start, tmax=end)
    
    # Reject stim channel
    raw.pick_types(eeg=True,  eog=True, stim=False, exclude='bads')
    
    # Filter for ICA
    if  Fmin and Fmax :
        l_freq = 1
        h_freq = 40
        raw.filter(l_freq=1, h_freq=40, fir_design='firwin', n_jobs=4)
    
    # Fit ICA
    if fit_ica or not op.isfile(ICA_file):
        ica = ICA(n_components=n_components, method=method) #create ICA object
        ica.exclude = []
        ica.drop_inds_ = []
        ica.labels_ = dict()
        ica.fit(raw, reject=ica_rejection, decim=2, picks=pick_types(raw.info, eeg=True)) #decimate: 200Hz is more than enough for ICA, saves time; picks: fit only on MEG
        ica.labels_['rejection'] = ica_rejection
        ica.labels_['drop_inds_'] = ica.drop_inds_
    else:
        ica = read_ica(ICA_file)
    
    # Detect EOG artifacts
   
    ica.labels_['eog_scores'] = ica.find_bads_eog(raw, threshold=EOG_threshold)[1].tolist()
    
    # Fix number of artifactual components
    ica.labels_['eog'] = ica.labels_['eog'][:EOG_max]
    if EOG_min and not ica.labels_['eog']:
        ica.labels_['eog'] = np.argsort(np.abs(ica.labels_['eog_scores'])).tolist()
        ica.labels_['eog'] = ica.labels_['eog'][::-1][:EOG_min]
    
    # Tag for exclusion
    ica.exclude = ica.labels_['eog']
    
    # Plot scores    
    ica.plot_scores(ica.labels_['eog_scores'], exclude=ica.labels_['eog'], labels='eog')
    if save:
        plt.savefig(ICA_eog_scores, transparent=True)
        plt.close()
    
    # Save ICA
    if save:
        ica.save(ICA_file)
        # Write ICA log
        #with open(ICA_log, 'a') as fid:
         #   fid.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(time.strftime('%Y_%m_%d\t%H:%M:%S',time.localtime()),rawdata_filepref,int(round(start)),int(round(end)) ,n_components,ica.n_components_,len(ica.labels_['eog']),ica.labels_['rejection'],len(ica.labels_['drop_inds_'])))
    
    return ica

def mmntask_rejtrial_get_P2P_amplitude(epochs, epoch_id, threshold):
    # print on screen for a selected epoch, the peak-to-peak amplitude value in uV for each sensor
    # and also those above threshold
    data_ep = epochs.get_data()
    tab = data_ep[epoch_id-1,:,:] # Nsens * Nsample
    mini = np.amin(tab, axis=1) # axis=1, minimum value for each raw
    maxi = np.amax(tab, axis=1)
    dyn = abs(maxi-mini)
    print('sensor', 'peak-tp-peak (uV)')
    for s, sens in enumerate(epochs.info['ch_names']):
        print(sens, round(dyn[s]*1000000))
    print('Above threshold',threshold*1000000 )
      
    ind = np.where(dyn>=threshold)[0] 
    
    for k in ind:
        print(epochs.info['ch_names'][k], round(dyn[k]*1000000))
        
    return ind
        

    
def mmntask_rejtrial_code_rej_events(epochs, events):
    res = epochs.drop_log
    ign, acc, rej = [], [], []
    for i,ep in enumerate(res):
        if ep == ['IGNORED']:
            ign.append(i)
            #print(i,ep)
        elif ep == []:
            acc.append(i)
            #print(i,ep)
        else:
            rej.append(i)
                
    ign=np.array(ign)
    acc=np.array(acc)
    rej=np.array(rej)
    rej_events = events.copy()
    rej_events[acc,2 ]=1
    rej_events[rej,2 ]=2
    rej_events[ign,2 ]=3
    return rej_events
    
      