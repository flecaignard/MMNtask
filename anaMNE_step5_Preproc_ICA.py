#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 10:25:26 2018

@author: francoise
"""

#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#
# STEP 5 : The aim here is to do ICA correction of blink (and saccade ?) artifacts


#
# assummes Step1 : Step4 have been conducted
# Requires *.resampled_renamed.raw-eve.fif and *.preproc_filt.raw.fif [events, raw]
#
# Output : *.refmasto_ica_filt_1-50.raw.fif
#
#
#
#https://www.martinos.org/mne/stable/auto_tutorials/plot_artifacts_correction_ica.html?highlight=ica
#
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import os.path as op
import time
import warnings


from mne import Epochs, pick_types, find_events, write_events, read_events, Annotations, read_selection
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import find_eog_events
from mne.preprocessing import ICA, read_ica
from mne.preprocessing import create_eog_epochs, create_ecg_epochs
from mne.datasets import sample


print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')
from mmntask_preproc import mmntask_runica_eeg

# from mmntask_events import mmntask_plot_events, mmntask_recog_condition, mmntask_recode_std

warnings.filterwarnings("ignore",category=DeprecationWarning)

# from oussama, allows plots in separate windows
matplotlib qt 

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',		'50023_MMTI_MMN', '50012 MMTI_MMN',		'50025_MMTI_MMN'] # supp of two datasets (uncompleted)
nr_subj = len(rawdata_filepref)


raw_eve_filename =[]              # List of event filenames (MNE *recode.raw-eve.fif),
raw_mne_filename =[]              # List of raw data filenames (MNE *.prep1.raw.fif)
new_raw_mne_filename =[] 
raw_ica_filepref =[] 
for i in range(nr_subj) :
    raw_eve_filename.append(os.path.join(dir_mne, '{}.resampled_renamed.raw-eve.fif'.format(rawdata_filepref[i])))
    raw_mne_filename.append(os.path.join(dir_mne, '{}.preproc_filt.raw.fif'.format(rawdata_filepref[i])))
    new_raw_mne_filename.append(os.path.join(dir_mne, '{}.refmasto_ica_filt_1-50.raw.fif'.format(rawdata_filepref[i])))
    raw_ica_filepref.append('{}.preproc_filt'.format(rawdata_filepref[i]))
print(raw_mne_filename)
print(raw_eve_filename)


#############################################################################################
## INIT                                         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# init color for 2D plots
dict_color_sens = dict( eeg='k', eog='blue', stim='m')
dict_color_event = {10: 'magenta', 11: 'black', 12: 'cyan', 20: 'red', 21: 'black', 22:'green', 65790:'yellow'}
dict_color_eog_event = {998: 'magenta'}

# init electrodes order for the viewer
order_eeg = np.array([1, 30, 2, 29, 3, 4, 31, 27, 28, 6,
                      5, 39, 26, 25, 7, 8, 32, 23, 24, 37,
                      10, 9, 22, 21, 38, 11, 12, 13, 19, 20,
                      14, 15,  16, 17, 18,40, 33, 34, 35, 36])
order_eeg = order_eeg -1

#############################################################################################
## Load one subject                             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################


id_su=0 #to be adjusted manually, as the screening of individual data is performed
    
f_raw = raw_mne_filename[id_su]
f_new_raw = new_raw_mne_filename[id_su]
f_eve = raw_eve_filename[id_su]
raw = read_raw_fif(f_raw, preload=True)
events =read_events(f_eve)
     
# launch viewer
raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = False, order=order_eeg, duration=30, highpass = None)

# get start and end boundaries of the ICA fit interval in seconds (from visual inspection, select one that comprises  a lot of blink and with high snr signals)
start = 11 # in sec, manually defined from visual inspection
end = 415

#############################################################################################
## ICA                            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
# from  MNE example + B. Ador function

ica = mmntask_runica_eeg(dir_mne, raw_ica_filepref[id_su],   start=start, end=end, save=True, fit_ica=True, n_components=0.975, method='fastica', ica_rejection={'eeg':20e-05}, EOG_threshold=3, EOG_min=1, EOG_max=2)
 
ica.plot_components()  # can you spot some potential bad guys?

compo_id = 0
ica.plot_properties(raw, picks=compo_id)
ica.plot_properties(raw, picks=[0, 8], psd_args={'fmax': 35.})

# plot component locked to  eog events
eog_epochs = create_eog_epochs(raw, reject=None)  # get single EOG trials
ica.plot_properties(eog_epochs, picks=[0], psd_args={'fmax': 35.},image_args={'sigma': 1.})

# # look at source time course, wit highlight on excluded ones
eog_average = eog_epochs.average().plot()
ica.plot_sources(eog_average, exclude=[0]) # does not work: ???

print(ica.labels_)
ica.labels_['eog']

# plot before /after correction
ica.plot_overlay(eog_average, exclude=[0], show=True) # does not work either: ???

# Apply the correction and save
# f_ica = os.path.join(dir_mne, 'ICA', '{}.components-ica.fif'.format(raw_ica_filepref[id_su]))
# ica = read_ica(f_ica)

new_raw = read_raw_fif(f_raw, preload=True)
ica.apply(new_raw)
new_raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = False, order=order_eeg, duration=30, highpass = None)
new_raw.save(fname=f_new_raw,overwrite=True)
