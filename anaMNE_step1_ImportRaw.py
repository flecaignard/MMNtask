#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#

#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

from mne import Epochs, pick_types, find_events, write_events, read_events
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events

print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')

from mmntask_events import mmntask_plot_events
#a()

matplotlib qt # from oussama, allows plots in separate windows

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_rawdata = '/Users/francoise/Documents/Projets/RickAdams/EEG_RawData'
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',	'50022_MMTI_MMN_T1',	'50023_MMTI_MMN', '50012 MMTI_MMN',	'50022_MMTI_MMN_T2-4',	'50025_MMTI_MMN']
nr_subj = len(rawdata_filepref)


rawdata_filename =[]          # List of raw data filenames (EEG *.bdf), Raw here refers to acquisition
raw_mne_filename =[]              # List of raw data filenames (MNE *raw.fif), raw here refers to MNE jargon
raw_eve_filename =[]              # List of event filenames (MNE *raw-eve.fif),
for i in range(nr_subj) :
    # rawdata_filename.append('{}/{}'.format(Dir_Raw,rawdata_filepref[i]))
    bdf_filename = '{}.bdf'.format(rawdata_filepref[i])
    rawdata_filename.append(os.path.join(dir_rawdata, bdf_filename))
    raw_mne_filename.append(os.path.join(dir_mne, '{}.raw.fif'.format(rawdata_filepref[i])))
    raw_eve_filename.append(os.path.join(dir_mne, '{}.raw-eve.fif'.format(rawdata_filepref[i])))

print(rawdata_filename)
print(raw_mne_filename)
print(raw_eve_filename)

#############################################################################################
## Import raw data into MNE (output = *.raw.fif)-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# help(read_raw_edf)
#Raw_List = [read_raw_edf(f, preload=True, stim_channel='auto') for f in  rawdata_filename] # Nice coding line :-)

# rawdatafile = rawdata_filename[0]
# raw = read_raw_edf(rawdatafile, eog=['VEO+', 'VEO-', 'HEOL', 'HEOR'],preload=True, stim_channel='Status') #stim_channel=40
# events = find_edf_events(raw) # empty, why, why?
# raw.info
# raw.ch_names
# picks = pick_types(raw.info, meg=False, eeg=False, stim=False, eog=True, exclude='bads')
# type(raw)

# Import montage 10-20
montage = read_montage('standard_1020')




for i,f in  enumerate(rawdata_filename) :
    print('Loading ....',i,f)
    raw = read_raw_edf(f, preload=True, stim_channel='Status', eog=['VEO+', 'VEO-', 'HEOL', 'HEOR'] )
    raw.set_montage(montage, set_dig=True, verbose=None)
    raw.save(raw_mne_filename[i], overwrite=True)
    print('-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
print('Load Raw Data : Done')


# check montage
raw.plot_sensors(show_names=True)
raw.plot_sensors('3d',show_names=True)

#############################################################################################
## Import events  (output = *.raw-eve.fif) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

# test on first dataset
# f = raw_mne_filename[1]
# raw = read_raw_fif(f, preload=True)
# raw.info
# raw.ch_names
# raw.plot()
# events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
# events.shape
# print(events)
#
# picks = pick_types(raw.info, meg=False, eeg=False, stim=True, eog=False, exclude='bads')
# raw.plot(events=events, order=picks, duration=60) # plot of stim channel only, thanks to 'order'
# type(events)
#
# events_code = np.unique(events[:,2]).tolist()   #list of the diffent event code found in events; tolist : from np.array to list
# event_id = {'std': 1, 'dev': 2, 'start': 65790, 's': 65536} #general, for all subjects
# event_id_indiv = {k:v for k, v in event_id.items() if v in events_code} #subset of event codes, adjusted for  current participant
#
#
# color_id = {1: 'green', 2: 'yellow', 65790: 'red', 65536: 'c'}
# color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
#
# plot_events(events, raw.info['sfreq'], raw.first_samp, color=color_id_indiv, event_id=event_id_indiv) # nice plot!
# write_events(raw_eve_filename[0],events)
#
event_id = {'std': 1, 'dev': 2, 'start': 65790, 's': 65536}
color_id = {1: 'green', 2: 'yellow', 65790: 'red', 65536: 'c'}

for i,f in  enumerate(raw_mne_filename) :
    raw = read_raw_fif(f, preload=True)
    events = find_events(raw, shortest_event=2, stim_channel='STI 014', output='onset')
    events_code = np.unique(events[:,2]).tolist()
    event_id_indiv = {k:v for k, v in event_id.items() if v in events_code}
    color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
    plot_events(events, raw.info['sfreq'], raw.first_samp, color=color_id_indiv, event_id=event_id_indiv)
    write_events(raw_eve_filename[i],events)
    print('-.-.-.-.-.-.-.-.-.-.-.   YOUPI   .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-')
print('Load Event Data : Done')

matplotlib qt
for i,f in  enumerate(raw_eve_filename) :
    events =read_events(f)
    # events_code = np.unique(events[:,2]).tolist()
    # event_id_indiv = {k:v for k, v in event_id.items() if v in events_code}
    # color_id_indiv = {k:v for k, v in color_id.items() if k in events_code}
    # plot_events(events, color=color_id_indiv, event_id=event_id_indiv)
    mmntask_plot_events(events)



#############################################################################################
#############################################################################################

# raw_fname = data_path + '/MEG/sample/sample_audvis_filt-0-40_raw.fif'
# event_fname = data_path + '/MEG/sample/sample_audvis_filt-0-40_raw-eve.fif'
# tmin, tmax = -0.1, 0.3
# event_id = dict(aud_l=1, aud_r=2, vis_l=3, vis_r=4)
#
# raw = mne.io.read_raw_fif(raw_fname, preload=True)
# raw.filter(1, 20, fir_design='firwin')
# events = mne.read_events(event_fname)
#
# picks = mne.pick_types(raw.info, meg=False, eeg=True, stim=False, eog=False,
#                        exclude='bads')
#
# epochs = mne.Epochs(raw, events, event_id, tmin, tmax, proj=False,
#                     picks=picks, baseline=None, preload=True,
#                     verbose=False)
#
# X = epochs.get_data()
