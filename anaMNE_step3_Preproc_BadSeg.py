#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#
# STEP 3 : The aim here is to view raw data with recoded events in order to
#           - detect bad sensors if any => store them raw.info['bads'] 
#           - check the quality of mastoid sensors for the re-referencing (not achieved in step 3)
#           - identify bad segments (e.g. EEG amplifier saturation) in mne.raw.annotations
#           - save bad sensors and bad segments (*.prep1.raw.fif)

#
# assummes Step1 : Step2 have been conducted
# Requires *.recode.raw-eve.fif and *.raw.fif [events, raw]
#
# Output : *.prep1.raw.fif 
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples

from mne import Epochs, pick_types, find_events, write_events, read_events, Annotations
from mne.channels import read_layout, Montage, read_montage
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events
from mne.preprocessing import find_eog_events


print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')
from mmntask_events import mmntask_plot_events, mmntask_recog_condition, mmntask_recode_std



# matplotlib qt # from oussama, allows plots in separate windows

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',		'50023_MMTI_MMN', '50012 MMTI_MMN',		'50025_MMTI_MMN'] # supp of two datasets (uncompleted)
nr_subj = len(rawdata_filepref)


raw_mne_filename =[]              # List of raw data filenames (MNE *raw.fif), raw here refers to MNE jargon
recode_raw_eve_filename =[]              # List of event filenames (MNE *recode.raw-eve.fif),
new_raw_mne_filename =[]              # List of raw data filenames (MNE *.prep1.raw.fif)
for i in range(nr_subj) :
    raw_mne_filename.append(os.path.join(dir_mne, '{}.raw.fif'.format(rawdata_filepref[i])))
    recode_raw_eve_filename.append(os.path.join(dir_mne, '{}.renamed.raw-eve.fif'.format(rawdata_filepref[i])))
    new_raw_mne_filename.append(os.path.join(dir_mne, '{}.preproc_bad.raw.fif'.format(rawdata_filepref[i])))
print(raw_mne_filename)
print(recode_raw_eve_filename)


#############################################################################################
## View raw data and recoded events -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# init color for 2D plots
dict_color_sens = dict( eeg='k', eog='blue', stim='m')
dict_color_event = {10: 'magenta', 11: 'black', 12: 'cyan', 20: 'red', 21: 'black', 22:'green', 65790:'yellow'}
dict_color_eog_event = {998: 'magenta'}

# init electrodes order for the viewer
order_eeg = np.array([1, 30, 2, 29, 3, 4, 31, 27, 28, 6,
                      5, 39, 26, 25, 7, 8, 32, 23, 24, 37,
                      10, 9, 22, 21, 38, 11, 12, 13, 19, 20,
                      14, 15,  16, 17, 18,40, 33, 34, 35, 36])
order_eeg = order_eeg -1

# init annotation attribute with a fake bad segments, so that it enables annotation viewer (key: a) in raw.plot
onset=[0]
duration=[0.1]
description = ['init_dummy']   
annotations = Annotations(onset, duration, description) 


matplotlib qt

############################# Now, check each subject ######################################
id_su=0 #to be adjusted manually, as the screening of individual data is performed
    
f_raw = raw_mne_filename[id_su]
f_eve = recode_raw_eve_filename[id_su]
f_new_raw = new_raw_mne_filename[id_su]
raw = read_raw_fif(f_raw)
events =read_events(f_eve)

raw.annotations = annotations

# launch viewer

raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=10, highpass = 2)

# Any bad sensors? to be marked on the viewer or manually
# raw.info['bads'] = ['MEG 2443', 'EEG 053']  
for i, ind in  enumerate(raw.info['bads']):
    print(ind)
    
# Any bad segments? to be marked from the annotation toolbox in the viewer
raw.annotations



# save
raw.save(fname=f_new_raw,overwrite=True)


