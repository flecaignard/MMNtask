#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 2018

@author: francoise
"""

#### MMNtask - preliminary analyses of EEG pilotes
# F. Lecaignard, April 17th, 2018
#
# STEP 6 : The aim here is to compute ERPs:
#           - filter 1-30 Hz
#           - rejection of bad trials
#           - compute (std, dev, dev-std) in condition U and E


#
# assummes Step1 : Step5 have been conducted
# Requires *.resampled_renamed.raw-eve.fif and *.refmasto_ica_filt_1-50.raw.fif [events, raw]
#
# Output : 
#
#############################################################################################
## Import packages          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
import numpy as np             # convention d'import
import os                       # path.join for file names
import matplotlib.pyplot as plt # from the MNE examples
import os.path as op
import time
import warnings


from mne import Epochs, Evoked,pick_types, find_events, write_events, read_events, Annotations, read_selection, read_epochs, combine_evoked, write_evokeds
from mne.channels import read_layout
from mne.io import concatenate_raws, read_raw_edf, read_raw_fif, find_edf_events
from mne.datasets import eegbci
from mne.decoding import CSP
from mne.viz import plot_events, plot_evoked_topo, plot_compare_evokeds
from mne.preprocessing import find_eog_events
from mne.preprocessing import ICA
from mne.preprocessing import create_eog_epochs, create_ecg_epochs
from mne.datasets import sample


print(__doc__)

import sys      # path to my own functions
sys.path.append('/Users/francoise/Documents/Projets/RickAdams/anaMNE/MMNtask_scripts')

from mmntask_preproc import mmntask_runica_eeg, mmntask_rejtrial_get_P2P_amplitude, mmntask_rejtrial_code_rej_events
# from mmntask_events import mmntask_plot_events, mmntask_recog_condition, mmntask_recode_std

warnings.filterwarnings("ignore",category=DeprecationWarning)

# from oussama, allows plots in separate windows
matplotlib qt 

#############################################################################################
## Declare paths            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################
dir_mne = '/Users/francoise/Documents/Projets/RickAdams/anaMNE/data'

#############################################################################################
## Subject List (class list) -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
#############################################################################################

rawdata_filepref = ['50002_MMTI_MMN',		'50023_MMTI_MMN', '50012 MMTI_MMN',		'50025_MMTI_MMN'] # supp of two datasets (uncompleted)
nr_subj = len(rawdata_filepref)


raw_eve_filename =[]              # List of event filenames (MNE *recode.raw-eve.fif),
raw_mne_filename =[]              # List of raw data filenames (MNE *.prep1.raw.fif)
rej_log_filename =[]
epochs_filename =[]             # List of epochs data filenames (MNE *.epo.fif)
evoked_filename =[]             # save (std, dev) *(U, E) evoked array (MNE *.-ave.fif)
Emmn_evoked_filename =[]             # save E mmn (dev -std) in *.-ave.fif) => temporary, I need to figure out how to append the evoked array object
Ummn_evoked_filename =[]             # save E mmn (dev -std) in *.-ave.fif) => temporary, I need to figure out how to append the evoked array object

for i in range(nr_subj) :
    raw_eve_filename.append(os.path.join(dir_mne, '{}.resampled_renamed.raw-eve.fif'.format(rawdata_filepref[i])))
    raw_mne_filename.append(os.path.join(dir_mne, '{}.refmasto_ica_filt_1-50.raw.fif'.format(rawdata_filepref[i])))
    rej_log_filename.append(os.path.join(dir_mne, '{}.rej_log.tsv'.format(rawdata_filepref[i])))
    epochs_filename.append(os.path.join(dir_mne, '{}.std_dev-epo.fif'.format(rawdata_filepref[i])))
    evoked_filename.append(os.path.join(dir_mne, '{}.std_dev_filt_1-30-ave.fif'.format(rawdata_filepref[i])))
    Emmn_evoked_filename.append(os.path.join(dir_mne, '{}.E_mmn_filt_1-30-ave.fif'.format(rawdata_filepref[i])))
    Ummn_evoked_filename.append(os.path.join(dir_mne, '{}.U_mmn_filt_1-30-ave.fif'.format(rawdata_filepref[i])))
print(raw_mne_filename)
print(raw_eve_filename)


#############################################################################################
## INIT                                         -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# init color for 2D plots
dict_color_sens = dict( eeg='k', eog='blue', stim='m')
dict_color_event = {10: 'magenta', 11: 'black', 12: 'cyan', 20: 'red', 21: 'black', 22:'green', 65790:'yellow'}
dict_color_eog_event = {998: 'magenta'}

# init electrodes order for the viewer
order_eeg = np.array([1, 30, 2, 29, 3, 4, 31, 27, 28, 6,
                      5, 39, 26, 25, 7, 8, 32, 23, 24, 37,
                      10, 9, 22, 21, 38, 11, 12, 13, 19, 20,
                      14, 15,  16, 17, 18,40, 33, 34, 35, 36])
order_eeg = order_eeg -1

# ERP bandwidth
fmin, fmax = 1, 30

#############################################################################################
## Load one subject                             -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################


id_su=0 #to be adjusted manually, as the screening of individual data is performed
    
f_raw = raw_mne_filename[id_su]
f_eve = raw_eve_filename[id_su]
f_rej_log = rej_log_filename[id_su]
f_epochs = epochs_filename[id_su]

f_evo = evoked_filename[id_su]
f_Emmn_evo = Emmn_evoked_filename[id_su]
f_Ummn_evo = Ummn_evoked_filename[id_su]


raw = read_raw_fif(f_raw, preload=True)
events =read_events(f_eve)
     
# launch viewer
raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=30, highpass = None)

#############################################################################################
## Filter 1-30      Hz                          -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
picks = pick_types(raw.info,  eeg=True, eog=True,stim=False, exclude='bads')
raw.filter(fmin,fmax, fir_design='firwin', picks=picks)

# launch viewer to control filter
raw.plot(events=events, color = dict_color_sens, event_color = dict_color_event,show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=30, highpass = None)

#############################################################################################
## Create epochs    (under threshold = T)       -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
picks = pick_types(raw.info,  eeg=True, eog=True,stim=False, exclude='bads')
event_id = {'E_std': 10,  'E_dev': 12,'U_std': 20,  'U_dev': 22} 
tmin, tmax = -0.2, 0.4
reject_tmin, reject_tmax = -0.15, 0.39

epochs=Epochs(raw, events, event_id=event_id, tmin=tmin, tmax=tmax, baseline=(None, 0), 
              picks=picks, preload=True, reject={'eeg':70e-06}, reject_tmin=tmin, reject_tmax=tmax,  reject_by_annotation=True, verbose=True)
epochs.plot_drop_log() # reject/sensor summary

#############################################################################################
## See rejected / accepted events  on raw,   (under T)    -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# 
# rej_event_id = {'accepted': 1,  'rejected': 2,'ignored': 3} 
rej_dict_color_event = {1: 'green', 2: 'red', 3: 'black'}
scal = dict(eeg=10e-5, eog=20e-5)

rej_events = mmntask_rejtrial_code_rej_events(epochs, events)
raw.plot(events=rej_events, color = dict_color_sens, event_color = rej_dict_color_event,scalings = scal,
         show_options=True, n_channels=raw.info['nchan'],remove_dc = True, order=order_eeg, duration=30)



# compute max-min range from a particular time interval (from MNE example)
win_s = 51,88 # in seconds, from raw.plot viewer
win_e = 52, 817
start, stop = raw.time_as_index([win_s, win_e])  # 
data, times = raw[:, start:stop] # does not work, to-do-list: debug this
print(data.shape)
print(times.shape)
data, times = raw[2:20:3, start:stop]  # access underlying data
raw.plot()



#############################################################################################
## View accepted epochs     (under T)                     -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

# view accepted epochs
# and possibly adjust threshold from viewing traces

#X = epochs.get_data()
#type(X)
dict_color_event = {10: 'magenta',  12: 'cyan', 20: 'red',  22:'green'}
scal = dict(eeg=20e-5, eog=150e-6)
epochs.plot(n_channels=raw.info['nchan'], n_epochs = 20, event_colors = dict_color_event, scalings=scal)


threshold = 70e-06 # uV
epoch_id = 86 # from epochs.plot()
mmntask_rejtrial_get_P2P_amplitude(epochs, epoch_id, threshold)


#############################################################################################
## Save epochs              (under T)                     -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
epochs.save(f_epochs)


#############################################################################################
## Compute ERP                                            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################
event_id = {'E_std': 10,  'E_dev': 12,'U_std': 20,  'U_dev': 22} 
dict_color_event = {10: 'magenta',  12: 'cyan', 20: 'red',  22:'green'}

epochs= read_epochs(f_epochs)

ERPs = [epochs[name].average() for name in event_id.keys()]
combine_evoked([ERPs[0], ERPs[1]], [-1, 1])

E_std = ERPs[0]
E_dev = ERPs[1]
U_std = ERPs[2]
U_dev = ERPs[3]
E_mmn = combine_evoked([E_std, E_dev], [-1, 1])
E_mmn.comment='E_mmn'
U_mmn = combine_evoked([U_std, U_dev], [-1, 1])
U_mmn.comment='U_mmn'

# save
write_evokeds(f_evo, ERPs)
write_evokeds(f_Emmn_evo, E_mmn)
write_evokeds(f_Ummn_evo, U_mmn)

#############################################################################################
## Compute ERP                                            -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
#############################################################################################

E_std=Evoked(f_evo, condition='E_std')
E_dev=Evoked(f_evo, condition='E_dev')
U_std=Evoked(f_evo, condition='U_std')
U_dev=Evoked(f_evo, condition='U_dev')
E_mmn = Evoked(f_Emmn_evo, condition='E_mmn')
U_mmn = Evoked(f_Ummn_evo, condition='U_mmn')

# different plots ...
title = 'Expected'
plot_evoked_topo([E_std, E_dev, E_mmn], color=['black', 'blue', 'green'], title=title, background_color='w')
title = 'Unexpected'
plot_evoked_topo([U_std, U_dev, U_mmn], color=['black', 'magenta', 'red'], title=title, background_color='w')

# sequences of maps spanning [50 250] ms
times = np.arange(0.05, 0.251, 0.02)
vmin, vmax =-2, 2
E_std.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='E_std')
E_dev.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='E_dev')
E_mmn.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='E_mmn')

U_std.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='U_std')
U_dev.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='U_dev')
U_mmn.plot_topomap(times=times, ch_type='eeg', vmin = vmin, vmax = vmax, title='U_mmn')


# butterfly plots with maps
ts_args = dict(gfp=True)
topomap_args = dict(sensors=True)
U_mmn.plot_joint(title='U MMN', times=np.arange(0.05, 0.251, 0.025),
                        ts_args=ts_args, topomap_args=topomap_args)

E_mmn.plot_joint(title='E MMN', times=np.arange(0.05, 0.251, 0.025),
                        ts_args=ts_args, topomap_args=topomap_args)

# zoom on specif sensors
for sens in ['Fz', 'FCz', 'Cz', 'Oz', 'Iz']:
    plot_compare_evokeds([U_mmn, E_mmn], picks=ERPs[0].ch_names.index(sens), colors=['red',  'green'])

